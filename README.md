# RUBYIZER
A simple algorithm to pick a random element from an array, then eliminate the item from the array with an interactive TUI. API coming soon. Modify $array decleration to add custom items: I use `$names = [*'a'..'z', *'A'..'Z', *'0'..'9']` by default, which adds all letters a-z uppercase and lowercase and the numbers 0-9. If you wanted items 'Gitlab', 'Github', and 'Bitbucket' for example, do:
```ruby
$names = ['Gitlab', 'Github', 'Bitbucket']
```
(Replace the first line with something like that.)