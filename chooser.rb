$names = ['a', 'b', 'c']
def choose(names)
	$choice = $names.sample
	puts $choice
	$names.delete($choice)
	$choice = nil
	if $names.empty? 
		puts "No more names left, quitting" 
		exit
	end
end
def yn
	print("Do you want to choose a random name from the list? ")
	yn = gets.chomp.to_s.strip.downcase
	yn = yn.strip
	if yn.include? 'y'
		choose($names)
	else
		exit
	end

end
loop do
	yn()
end
